package dev.kourosh.kiliaro.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import dev.kourosh.kiliaro.data.db.daos.ImageDao
import dev.kourosh.kiliaro.data.db.entities.ImageInfo

@Database(entities = [ImageInfo::class], version = 1)
abstract class KiliaroDb : RoomDatabase() {
    abstract fun imageDao(): ImageDao
}
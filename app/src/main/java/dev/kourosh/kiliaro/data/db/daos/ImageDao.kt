package dev.kourosh.kiliaro.data.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import dev.kourosh.kiliaro.data.db.entities.ImageInfo

@Dao
interface ImageDao {

    @Query("SELECT * FROM imageinfo_tbl")
    fun findAllLiveData(): LiveData<List<ImageInfo>>

    @Query("SELECT * FROM imageinfo_tbl")
    fun findAll(): List<ImageInfo>

    @Query("SELECT id FROM imageinfo_tbl WHERE id in (:ids)")
    fun findIds(ids: Set<String>): List<String>

    @Query("SELECT * FROM imageinfo_tbl WHERE id=:id")
    fun findById(id: String): LiveData<ImageInfo?>

    @Query("UPDATE imageinfo_tbl SET localSavedPath=:imageFilePath WHERE id=:id")
    fun updateFilePath(id: String, imageFilePath: String)

    @Transaction
    fun reloadAllImages(imageInfoList: List<ImageInfo>) {
        val ids = imageInfoList.map { it.id }.toSet()
        val savedIds = if (ids.isNotEmpty()) findIds(ids) else listOf()
        if (savedIds.isNotEmpty()) {
            deleteNotInIds(savedIds.toSet())
        }
        val newImages=imageInfoList.filter { !savedIds.contains(it.id) }
        if (newImages.isNotEmpty()) {
            insertAll(newImages)
        }
    }

    @Insert
    fun insertAll(imageInfoList: List<ImageInfo>)

    @Query("DELETE FROM imageinfo_tbl WHERE id NOT IN (:ids)")
    fun deleteNotInIds(ids: Set<String>)

    @Query("DELETE FROM imageinfo_tbl")
    fun deleteAll()
}
package dev.kourosh.kiliaro.data.utils

import java.io.BufferedInputStream
import java.io.File
import java.io.InputStream

fun InputStream.saveInFile(file: File): Long {
    BufferedInputStream(this).use { input ->
        val bytes=input.readBytes()
        file.writeBytes(bytes)
        return file.length()
    }
}
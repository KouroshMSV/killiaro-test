package dev.kourosh.kiliaro.data.interactors.impl

import dev.kourosh.kiliaro.crosscutting.withIoContext
import dev.kourosh.kiliaro.data.db.KiliaroDb
import dev.kourosh.kiliaro.data.network.adapters.KiliaroRestAdapter
import dev.kourosh.kiliaro.domain.Repository


class ReloadSharedMedias(
    private val sharedMediaAdapter: KiliaroRestAdapter,
    private val db: KiliaroDb,
) : Repository.ReLoadImagesInfoList {

    override suspend fun invoke(parameter: Unit): Result<Unit> {
        return sharedMediaAdapter.findSharedImages().map {
            withIoContext {
                db.imageDao().reloadAllImages(it.map { it.toEntity() })
            }
        }
    }

}


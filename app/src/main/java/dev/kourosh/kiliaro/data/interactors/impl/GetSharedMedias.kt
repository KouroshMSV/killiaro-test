package dev.kourosh.kiliaro.data.interactors.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import dev.kourosh.kiliaro.data.db.KiliaroDb
import dev.kourosh.kiliaro.domain.Repository
import dev.kourosh.kiliaro.domain.entities.ImageInfo


class GetSharedMedias(
    private val db: KiliaroDb
) : Repository.GetImagesInfoList {

    override suspend fun invoke(parameter: Unit): LiveData<List<ImageInfo>> {
        return db.imageDao().findAllLiveData().map { it.map { it.toDomain() } }

    }


}


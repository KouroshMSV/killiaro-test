package dev.kourosh.kiliaro.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

@Entity(tableName = "imageinfo_tbl")
data class ImageInfo(
    @PrimaryKey  val id: String,
    val userId: String,
    val mediaType: String,
    val filename: String,
    val size: Long,
    val createdAt: String,
    val takenAt: String?,
    val guessedTakenAt: String?,
    val contentType: String,
    val thumbnailUrl: String,
    val downloadUrl: String,
    val resx: Long,
    val resy: Long,
    val localSavedPath: String?
){
    fun toDomain() = dev.kourosh.kiliaro.domain.entities.ImageInfo(
        id = id,
        userId = userId,
        mediaType = mediaType,
        filename = filename,
        size = size,
        createdAt = LocalDateTime.parse(createdAt, DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        takenAt = takenAt,
        guessedTakenAt = guessedTakenAt,
        contentType = contentType,
        thumbnailUrl = thumbnailUrl,
        downloadUrl = downloadUrl,
        resx = resx,
        resy = resy,
        localSavedPath = localSavedPath,
    )

}
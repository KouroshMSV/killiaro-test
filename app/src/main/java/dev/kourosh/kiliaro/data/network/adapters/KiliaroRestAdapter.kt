package dev.kourosh.kiliaro.data.network.adapters

import dev.kourosh.kiliaro.data.models.response.ImageInfo

interface KiliaroRestAdapter {

    suspend fun findSharedImages(
    ): Result<List<ImageInfo>>
}
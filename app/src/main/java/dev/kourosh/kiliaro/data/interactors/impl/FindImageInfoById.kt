package dev.kourosh.kiliaro.data.interactors.impl

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import dev.kourosh.kiliaro.crosscutting.withIoContext
import dev.kourosh.kiliaro.data.db.KiliaroDb
import dev.kourosh.kiliaro.domain.Repository
import dev.kourosh.kiliaro.domain.entities.ImageInfo


class FindImageInfoById(
    private val db: KiliaroDb,
) : Repository.FindImageInfoById {
    override suspend fun invoke(parameter: String): LiveData<ImageInfo?> {
        return withIoContext { db.imageDao().findById(parameter).map { it?.toDomain() } }
    }
}


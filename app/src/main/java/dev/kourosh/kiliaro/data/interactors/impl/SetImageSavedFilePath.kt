package dev.kourosh.kiliaro.data.interactors.impl

import dev.kourosh.kiliaro.crosscutting.withIoContext
import dev.kourosh.kiliaro.data.db.KiliaroDb
import dev.kourosh.kiliaro.domain.Repository
import dev.kourosh.kiliaro.domain.model.request.ImageIdFilePath


class SetImageSavedFilePath(
    private val db: KiliaroDb,
) : Repository.SetImageSavedFilePathInteractor {
    override suspend fun invoke(parameter: ImageIdFilePath) {
        return withIoContext { db.imageDao().updateFilePath(parameter.imageId, parameter.filePath) }
    }
}


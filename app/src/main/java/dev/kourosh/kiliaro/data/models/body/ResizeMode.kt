package dev.kourosh.kiliaro.data.models.body

/**
 * "bb" for bounding box. Original aspect ratio is maintained and the image is resized to completely fit inside the new width and height.
 *
 * "crop" for cropped. Original aspect ratio is maintained and the image is stretched to fill the new width and height completely. The parts of the image that are outside the specified box will be removed.
 *
 * "md" for minimum dimensions. Same as crop but the parts of the image that are outside the specified box will be kept.
 */
enum class ResizeMode(val key: String) {
    BOUNDING_BOX("bb"),
    CROPPED("crop"),
    MINIMUM_DIMENSIONS("md"),
}
package dev.kourosh.kiliaro.data.network.adapters

import dev.kourosh.kiliaro.crosscutting.withIoContext
import dev.kourosh.kiliaro.data.models.response.ImageInfo
import dev.kourosh.kiliaro.data.network.endpoints.KiliaroEndpoints
import retrofit2.HttpException

class KiliaroRestAdapterImpl(private val kiliaroEndpoints: KiliaroEndpoints, private val token: String) : KiliaroRestAdapter {
    override suspend fun findSharedImages(): Result<List<ImageInfo>> {
        return withIoContext {
            try {
                Result.success(kiliaroEndpoints.findSharedImages(sharedKey = token))
            } catch (e: HttpException) {
                Result.failure(e)
            } catch (e: Exception) {
                Result.failure(e)
            }
        }

    }
}
package dev.kourosh.kiliaro.data.network.endpoints

import dev.kourosh.kiliaro.data.models.response.ImageInfo
import retrofit2.http.GET
import retrofit2.http.Path

interface KiliaroEndpoints {

    @GET("/shared/{sharedKey}/media")
    suspend fun findSharedImages(
        @Path("sharedKey") sharedKey: String,
    ): List<ImageInfo>
}
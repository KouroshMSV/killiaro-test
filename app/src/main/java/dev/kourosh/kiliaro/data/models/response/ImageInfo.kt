package dev.kourosh.kiliaro.data.models.response


import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter


@Serializable
data class ImageInfo(
    val id: String,
    @SerialName("user_id")
    val userId: String,
    @SerialName("media_type")
    val mediaType: String,
    val filename: String,
    val size: Long,
    @Contextual
    @SerialName("created_at")
    val createdAt: LocalDateTime,
    @SerialName("taken_at")
    val takenAt: String?,
    @SerialName("guessed_taken_at")
    val guessedTakenAt: String?,
    val md5sum: String,
    @SerialName("content_type")
    val contentType: String,
//        val video: Any?,
    @SerialName("thumbnail_url")
    val thumbnailUrl: String,
    @SerialName("download_url")
    val downloadUrl: String,
    val resx: Long,
    val resy: Long
) {
    fun toEntity() = dev.kourosh.kiliaro.data.db.entities.ImageInfo(
        id = id,
        userId = userId,
        mediaType = mediaType,
        filename = filename,
        size = size,
        createdAt = createdAt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        takenAt = takenAt,
        guessedTakenAt = guessedTakenAt,
        contentType = contentType,
        thumbnailUrl = thumbnailUrl,
        downloadUrl = downloadUrl,
        resx = resx,
        resy = resy,
        localSavedPath = null,
    )
}

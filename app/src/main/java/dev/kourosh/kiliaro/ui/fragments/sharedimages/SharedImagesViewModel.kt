package dev.kourosh.kiliaro.ui.fragments.sharedimages

import androidx.lifecycle.ViewModel
import dev.kourosh.kiliaro.domain.Repository

class SharedImagesViewModel(
    val getImagesInfoListInteractor: Repository.GetImagesInfoList,
    val reLoadImagesInfoListInteractor: Repository.ReLoadImagesInfoList
) : ViewModel() {
    val showImagesAdapter = ShowImagesAdapter()
//    val getNewImagesLoading = SingleLiveEvent<Boolean>()

}
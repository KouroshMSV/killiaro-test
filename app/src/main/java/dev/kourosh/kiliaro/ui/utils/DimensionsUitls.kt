package dev.kourosh.kiliaro.ui.utils

import android.content.res.Resources

data class Dp(val value: Int) {
    init {
        require(value >=0)
    }
    fun toPx():Px {
        return Px((value * Resources.getSystem().displayMetrics.density).toInt())
    }
}

data class Px(val value: Int) {
    init {
        require(value >=0)
    }
    fun toDP():Dp {
        return Dp((value / Resources.getSystem().displayMetrics.density).toInt())
    }
}

val Int.dp: Dp
    get() = Dp(this)
val Int.px: Px
    get() = Px(this)

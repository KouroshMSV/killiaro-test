package dev.kourosh.kiliaro.ui.utils

import org.threeten.bp.format.DateTimeFormatter

val dateTimeFormatter: DateTimeFormatter =DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
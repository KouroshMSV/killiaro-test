package dev.kourosh.kiliaro.ui.infrastructure

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

object CustomSerializer {

    @Serializer(forClass = LocalDateTime::class)
    object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
        override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("org.threeten.bp.LocalDateTime", PrimitiveKind.LONG)
        override fun serialize(encoder: Encoder, value: LocalDateTime) {
            encoder.encodeString(value.format(DateTimeFormatter.ISO_DATE_TIME))
        }

        override fun deserialize(decoder: Decoder): LocalDateTime {
            return LocalDateTime.parse(decoder.decodeString(), DateTimeFormatter.ISO_DATE_TIME)
        }
    }
}
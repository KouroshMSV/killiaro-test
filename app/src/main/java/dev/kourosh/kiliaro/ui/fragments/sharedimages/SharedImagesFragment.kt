package dev.kourosh.kiliaro.ui.fragments.sharedimages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.TooltipCompat
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import dev.kourosh.kiliaro.databinding.SharedImagesFragmentBinding
import dev.kourosh.kiliaro.ui.infrastructure.BaseFragment
import dev.kourosh.kiliaro.ui.models.ImageInfo
import org.koin.androidx.viewmodel.ext.android.viewModel


class SharedImagesFragment : BaseFragment() {
    private val viewModel by viewModel<SharedImagesViewModel>()
    private lateinit var binding: SharedImagesFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        return SharedImagesFragmentBinding.inflate(inflater, container, false).let {
            binding = it
            it.root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.rclImages) {
            adapter = viewModel.showImagesAdapter
            layoutManager = StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL).apply {
                gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
            }
            setHasFixedSize(false)
        }
        with(binding.fabReloadImages) {
            TooltipCompat.setTooltipText(this, contentDescription)
            setOnClickListener {
                loadNewImages()
            }
        }
    }

    override fun init() {
        var loadedImagesForFirstTime = false
        launchOnMainContext {
            viewModel.getImagesInfoListInteractor.invoke(Unit).observe {imageList->
                if (imageList.isEmpty() && !loadedImagesForFirstTime) {
                    loadedImagesForFirstTime = true
                    loadNewImages()
                }
                viewModel.showImagesAdapter.replaceAllImages(imageList.map { ImageInfo.fromDomain(it) })
            }
        }

        viewModel.showImagesAdapter.onImageClicked { imageView, imageInfo ->
            findNavController().navigate(
                SharedImagesFragmentDirections.actionSharedImagesFragmentToShowFullscreenImageFragment(imageInfo.id),
                FragmentNavigatorExtras(imageView to imageInfo.id)
            )
        }
    }

    private fun loadNewImages() {
        showLoading(true)
        launchOnMainContext {
            viewModel.reLoadImagesInfoListInteractor.invoke(Unit).fold({
                showLoading(false)
                showMessage("images loaded")
            }, {
                showLoading(false)
                showMessage( it.message ?: "unknown error")
            })
        }
    }

    private fun showLoading(show: Boolean) {
        when (show) {
            true -> {
                binding.fabReloadImages.isEnabled = false
                binding.prgReloadImages.isEnabled = true
                binding.prgReloadImages.visibility = View.VISIBLE
            }
            false -> {
                binding.fabReloadImages.isEnabled = true
                binding.prgReloadImages.isEnabled = false
                binding.prgReloadImages.visibility = View.GONE
            }
        }
    }

}
package dev.kourosh.kiliaro.ui.utils

import android.Manifest
import android.app.Activity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker

object PermissionUtils {
    enum class Permission(val permissionName: String) {
        WRITE_EXTERNAL_STORAGE(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }

    fun checkPermission(activity: Activity, permission: Permission, requestCode: Int = 101, granted: () -> Unit, shouldShowRequestPermissionRationale: () -> Unit) {
        when {
            ActivityCompat.checkSelfPermission(activity, permission.permissionName) == PermissionChecker.PERMISSION_GRANTED -> granted()
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permission.permissionName) -> shouldShowRequestPermissionRationale()
            else -> requestPermission(activity, permission, requestCode)

        }
    }

    fun requestPermission(activity: Activity, permission: Permission, requestCode: Int = 101) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission.permissionName), requestCode)
    }
}


package dev.kourosh.kiliaro.ui.fragments.showfullscreenimage

import androidx.lifecycle.ViewModel
import dev.kourosh.kiliaro.domain.Repository

class ShowFullscreenImageViewModel(val findImageInfoByIdInteractor: Repository.FindImageInfoById, val updateFilePathInteractor: Repository.SetImageSavedFilePathInteractor) : ViewModel() {

}
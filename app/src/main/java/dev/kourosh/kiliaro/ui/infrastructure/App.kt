package dev.kourosh.kiliaro.ui.infrastructure

import android.app.Application
import androidx.room.Room
import com.jakewharton.threetenabp.AndroidThreeTen
import dev.kourosh.kiliaro.data.db.KiliaroDb
import dev.kourosh.kiliaro.data.interactors.impl.FindImageInfoById
import dev.kourosh.kiliaro.data.interactors.impl.GetSharedMedias
import dev.kourosh.kiliaro.data.interactors.impl.ReloadSharedMedias
import dev.kourosh.kiliaro.data.interactors.impl.SetImageSavedFilePath
import dev.kourosh.kiliaro.data.network.ApiService
import dev.kourosh.kiliaro.data.network.adapters.KiliaroRestAdapter
import dev.kourosh.kiliaro.data.network.adapters.KiliaroRestAdapterImpl
import dev.kourosh.kiliaro.domain.Repository
import dev.kourosh.kiliaro.ui.fragments.sharedimages.SharedImagesViewModel
import dev.kourosh.kiliaro.ui.fragments.showfullscreenimage.ShowFullscreenImageViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.GlobalContext.startKoin
import org.koin.core.qualifier.qualifier
import org.koin.dsl.module
import java.util.concurrent.Executors

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        initDI()
    }

    private fun initDI() {
        startKoin {
            androidLogger()
            androidContext(this@App)
            val infrastructureModule = module(createdAtStart = true) {
                single {
                    Json {
                        ignoreUnknownKeys = true
                        allowStructuredMapKeys = true
                        encodeDefaults = true
                        allowSpecialFloatingPointValues = true
                        serializersModule = SerializersModule {
                            contextual(CustomSerializer.LocalDateTimeSerializer)
                        }
                    }
                }
                single {
                    Room.databaseBuilder(applicationContext, KiliaroDb::class.java, "kiliaro_db").build()
                }
                single<CoroutineScope>(qualifier("sharedCoroutineScope")) {
                    CoroutineScope(Executors.newFixedThreadPool(3).asCoroutineDispatcher())
                }
            }
            val dataModule = module(createdAtStart = true) {
                single<KiliaroRestAdapter> {
                    KiliaroRestAdapterImpl(
                        kiliaroEndpoints = ApiService(
                            url = "https://api1.kiliaro.com/",
                            connectTimeout = 10,
                            readWriteTimeout = 60,
                            jsonSerializer = get(),
                        ).create(),
                        "djlCbGusTJamg_ca4axEVw"
                    )
                }
            }

            val interactorsModule = module {
                single<Repository.GetImagesInfoList> {
                    GetSharedMedias(
                        db = get()
                    )
                }
                single<Repository.FindImageInfoById> {
                    FindImageInfoById(db = get())
                }
                single<Repository.ReLoadImagesInfoList> {
                    ReloadSharedMedias(
                        sharedMediaAdapter = get(),
                        db = get()
                    )
                }
                single<Repository.SetImageSavedFilePathInteractor> {
                    SetImageSavedFilePath(
                        db = get()
                    )
                }
            }

            val viewModelModules = module {
                viewModel {
                    SharedImagesViewModel(
                        getImagesInfoListInteractor = get(),
                        reLoadImagesInfoListInteractor = get()
                    )
                }
                viewModel {
                    ShowFullscreenImageViewModel(
                        findImageInfoByIdInteractor = get(),
                        updateFilePathInteractor = get()
                    )
                }
            }

            modules(
                infrastructureModule,
                dataModule,
                interactorsModule,
                viewModelModules
            )
        }
    }

}
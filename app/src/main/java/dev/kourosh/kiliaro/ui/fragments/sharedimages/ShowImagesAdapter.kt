package dev.kourosh.kiliaro.ui.fragments.sharedimages

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import dev.kourosh.kiliaro.databinding.ItemImageBinding
import dev.kourosh.kiliaro.ui.models.ImageInfo
import dev.kourosh.kiliaro.ui.utils.dp
import dev.kourosh.kiliaro.ui.utils.loadWithUrl
import dev.kourosh.kiliaro.ui.utils.px

class ShowImagesAdapter : RecyclerView.Adapter<ShowImagesAdapter.Holder>() {
    private var onImageClickListener: OnItemClickListener? = null
    private val imageInfoList = mutableListOf<ImageInfo>()
    private val itemWidth: Int by lazy {
        val padding = 12.dp.toPx().value * 2
        val colCount = 3
        val totalMargin = 8.dp.toPx().value * colCount
        val screenWidth = context.resources.displayMetrics.widthPixels
        val widthPx = ((screenWidth - totalMargin - padding) / colCount).px
        widthPx.toDP().value
    }
    private lateinit var context: Context


    fun onImageClicked(listener: (imageView: ImageView, imageInfo: ImageInfo) -> Unit) {
        onImageClickListener = object : OnItemClickListener {
            override fun onClick(view: ImageView, imageInfo: ImageInfo) {
                listener(view, imageInfo)
            }
        }
    }

    fun replaceAllImages(newImages: List<ImageInfo>) {
        val diffCallback = ImageInfoCallback(imageInfoList, newImages)
        DiffUtil.calculateDiff(diffCallback).apply {
            dispatchUpdatesTo(this@ShowImagesAdapter)
        }

        imageInfoList.clear()
        imageInfoList.addAll(newImages)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        context = parent.context
        return Holder(ItemImageBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val imageInfo = imageInfoList[position]
        val imageUrl = imageInfo.thumbnailUrl.let { "$it?w=$itemWidth" }

        ConstraintSet().apply {
            val ratio = "${imageInfo.resx}:${imageInfo.resy}"
            clone(holder.binding.constraintLayout)
            setDimensionRatio(holder.binding.image.id, ratio)
            applyTo(holder.binding.constraintLayout)
        }
        with(holder.binding.image) {
            transitionName = imageInfo.id
            loadWithUrl(context, imageUrl, CircularProgressDrawable(context).apply {
                strokeWidth = 5f
                centerRadius = 30f
                start()
            })
            setOnClickListener {
                onImageClickListener?.onClick(
                    it as ImageView,
                    imageInfoList[holder.adapterPosition]
                )
            }
        }
    }

    override fun getItemCount() = imageInfoList.size

    class Holder(val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root)
    interface OnItemClickListener {
        fun onClick(view: ImageView, imageInfo: ImageInfo)
    }

    private class ImageInfoCallback(private val oldList: List<ImageInfo>, private val newList: List<ImageInfo>) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id === newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldImageInfo: Int, newPosition: Int): Boolean {
            return oldList[oldImageInfo] == newList[newPosition]
        }

    }
}
package dev.kourosh.kiliaro.ui.utils

import android.os.Environment
import dev.kourosh.kiliaro.crosscutting.withIoContext
import dev.kourosh.kiliaro.data.utils.saveInFile
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File


object FileUtils {
    private val client = OkHttpClient()

    /**
     * @param name name of my file with extension
     * @param url you want to download from that url
     * @return filePath
     * */
    suspend fun download(name: String, url: String, directory: File = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)): Result<String> {
        return try {
            if (!directory.exists()) {
                directory.mkdir()
            }
            val request = Request.Builder()
                .url(url)
                .build()
            withIoContext {
                client.newCall(request).execute().use { response ->
                    if (response.isSuccessful) {
                        val file = File(directory, name)
                        response.body?.use { body ->
                            body.byteStream().saveInFile(file)
                            body.close()
                        } ?: return@use Result.failure(Exception("could not download image"))
                        Result.success(file.path)
                    } else {
                        Result.failure(Exception("could not download image"))
                    }
                }
            }
        } catch (e: Exception) {
            Result.failure(e)
        }
    }
}
package dev.kourosh.kiliaro.ui.infrastructure

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import dev.kourosh.kiliaro.crosscutting.launchOnMainContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

abstract class BaseFragment : Fragment() {
    private val TAG = javaClass.simpleName
    private var viewInitialized = false
    open fun init() {}
    protected val viewCoroutineScope: CoroutineScope by lazy {
        viewLifecycleOwner.lifecycleScope
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!viewInitialized) {
            init()
            viewInitialized = true
        }
    }

    inline fun <T> LiveData<T>.observe(crossinline observer: (T) -> Unit) {
        observe(viewLifecycleOwner) {
            observer(it)
        }
    }

    fun launchOnMainContext(block: suspend CoroutineScope.() -> Unit): Job {
        return viewCoroutineScope.launchOnMainContext { block() }
    }

    fun showMessage(message: String) {
        view?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_SHORT).show()

        }
    }
}
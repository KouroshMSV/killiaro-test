package dev.kourosh.kiliaro.ui.models

import org.threeten.bp.LocalDateTime

data class ImageInfo(
    val id: String,
    val mediaType: String,
    val filename: String,
    val size: Long,
    val createdAt: LocalDateTime,
    val contentType: String,
    val thumbnailUrl: String,
    val downloadUrl: String,
    val resx: Long,
    val resy: Long,
    val localSavedPath: String?
)  {
    companion object {
        fun fromDomain(imageInfo: dev.kourosh.kiliaro.domain.entities.ImageInfo): ImageInfo {
            return ImageInfo(
                id = imageInfo.id,
                mediaType = imageInfo.mediaType,
                filename = imageInfo.filename,
                size = imageInfo.size,
                createdAt = imageInfo.createdAt,
                contentType = imageInfo.contentType,
                thumbnailUrl = imageInfo.thumbnailUrl,
                downloadUrl = imageInfo.downloadUrl,
                resx = imageInfo.resx,
                resy = imageInfo.resy,
                localSavedPath = imageInfo.localSavedPath,
            )

        }
    }
}
package dev.kourosh.kiliaro.ui.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

fun ImageView.loadWithUrl(
    context: Context,
    imageUrl: String,
    placeHolder: Drawable? = null,
) {

    Log.d("loadingImage", imageUrl)
    Glide.with(context)
        .load(Uri.parse(imageUrl)).run {
            if (placeHolder != null)
                placeholder(placeHolder)
            else
                this
        }
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .skipMemoryCache(true)
        .into(this)

}

package dev.kourosh.kiliaro.ui.fragments.showfullscreenimage

import android.content.res.ColorStateList
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.TooltipCompat
import androidx.core.content.ContextCompat
import androidx.core.view.*
import androidx.navigation.fragment.navArgs
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.google.android.material.snackbar.Snackbar
import dev.kourosh.kiliaro.R
import dev.kourosh.kiliaro.databinding.ShowFullscreenImageFragmentBinding
import dev.kourosh.kiliaro.domain.model.request.ImageIdFilePath
import dev.kourosh.kiliaro.ui.infrastructure.BaseFragment
import dev.kourosh.kiliaro.ui.models.ImageInfo
import dev.kourosh.kiliaro.ui.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class ShowFullscreenImageFragment : BaseFragment() {
    private val args by navArgs<ShowFullscreenImageFragmentArgs>()

    private val viewModel by viewModel<ShowFullscreenImageViewModel>()
    private lateinit var binding: ShowFullscreenImageFragmentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return ShowFullscreenImageFragmentBinding.inflate(inflater, container, false).let {
            binding = it
            it.root
        }
    }

    private val windowInsetsController: WindowInsetsControllerCompat? by lazy { ViewCompat.getWindowInsetsController(requireView()) }
    private var imageInfo: ImageInfo? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideSystemBars()
        saveImageLoading(false)
        launchOnMainContext {
            viewModel.findImageInfoByIdInteractor.invoke(args.imageId).observe { imageInfoDomain ->
                imageInfo=imageInfoDomain?.let { ImageInfo.fromDomain(it)}
                val imageInfo=imageInfo
                if (imageInfo == null) {
                    requireActivity().onBackPressed()
                } else {
                    with(binding.image) {
                        transitionName = imageInfo.id
                        val screenWidth = context.resources.displayMetrics.widthPixels.px.toDP().value
                        val url = "${imageInfo.thumbnailUrl}?w=$screenWidth"
                        loadWithUrl(requireContext(), url, CircularProgressDrawable(context).apply {
                            setColorSchemeColors(ContextCompat.getColor(context, R.color.white))
                            strokeWidth = 5f
                            centerRadius = 30f
                            start()
                        })
                        setOnClickListener {
                            if (isSystemBarsHidden) {
                                showSystemBars()
                            } else {
                                hideSystemBars()
                            }
                        }
                    }
                    with(binding.txtTakenDateTime) {
                        TooltipCompat.setTooltipText(this, contentDescription)
                        text = imageInfo.createdAt.format(dateTimeFormatter)
                    }
                    with(binding.imgTakenDateTime) {
                        TooltipCompat.setTooltipText(this, contentDescription)
                    }


                    with(binding.imgSaveImage) {
                        TooltipCompat.setTooltipText(this, contentDescription)
                        if (imageInfo.localSavedPath != null && File(imageInfo.localSavedPath).isFile) {
                            changeDownloadedImage(imageInfo.localSavedPath)
                        } else {
                            changeDownloadedImage(imageInfo.localSavedPath)
                            setOnClickListener {
                                PermissionUtils.checkPermission(requireActivity(), PermissionUtils.Permission.WRITE_EXTERNAL_STORAGE,
                                    granted = {
                                        launchOnMainContext {
                                            saveImageLoading(true)
                                            //we can add to service for download image in background
                                            FileUtils.download(imageInfo.filename, imageInfo.downloadUrl).fold({ filePath ->
                                                viewModel.updateFilePathInteractor.invoke(ImageIdFilePath(imageInfo.id, filePath))
                                                showMessage("image saved")
                                                saveImageLoading(false)
                                            }, {
                                                showMessage(it.message ?: "unknown message")
                                                saveImageLoading(false)
                                            })
                                        }
                                    },
                                    shouldShowRequestPermissionRationale = {
                                        Snackbar.make(view, "we need file access for saving image", Snackbar.LENGTH_INDEFINITE)
                                            .setAction("OK") {
                                                PermissionUtils.requestPermission(requireActivity(), PermissionUtils.Permission.WRITE_EXTERNAL_STORAGE)
                                            }.show()
                                    })
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        changeDownloadedImage(imageInfo?.localSavedPath)
    }

    private fun changeDownloadedImage(filePath: String?) {
        when ((filePath != null && File(filePath).isFile)) {
            true -> binding.imgSaveImage.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.teal_700))
            false -> binding.imgSaveImage.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.white))
        }
    }

    private fun saveImageLoading(loading: Boolean) {
        when (loading) {
            true -> {
                binding.imgSaveImage.isGone = true
                binding.prgSaveImage.isVisible = true
            }
            false -> {

                binding.imgSaveImage.isVisible = true
                binding.prgSaveImage.isGone = true
            }
        }
    }

    override fun onDestroyView() {
        showSystemBars()
        super.onDestroyView()
    }

    private var isSystemBarsHidden = false

    private fun hideSystemBars() {
        windowInsetsController?.let { windowInsetsController ->
            isSystemBarsHidden = true
            windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
        }
    }

    private fun showSystemBars() {
        windowInsetsController?.let { windowInsetsController ->
            isSystemBarsHidden = false
            windowInsetsController.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            windowInsetsController.show(WindowInsetsCompat.Type.systemBars())
        }
    }
}
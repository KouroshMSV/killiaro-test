package dev.kourosh.kiliaro.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dev.kourosh.kiliaro.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
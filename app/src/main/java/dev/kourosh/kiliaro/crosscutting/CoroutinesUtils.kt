package dev.kourosh.kiliaro.crosscutting

import kotlinx.coroutines.*

suspend fun <T> withIoContext( block:suspend CoroutineScope.() -> T):T{
    return withContext(ioDispatcher){
        block()
    }
}
fun <T> CoroutineScope.launchOnMainContext(block: suspend CoroutineScope.() -> T): Job {
    return launch(mainDispatcher){
        block()
    }
}
val ioDispatcher=Dispatchers.IO
val mainDispatcher=Dispatchers.Main
package dev.kourosh.kiliaro.domain.model.request

data class ImageIdFilePath(val imageId: String, val filePath: String)
package dev.kourosh.kiliaro.domain

import androidx.lifecycle.LiveData
import dev.kourosh.kiliaro.domain.entities.ImageInfo
import dev.kourosh.kiliaro.domain.model.request.ImageIdFilePath

sealed interface Repository<T, R> {
    suspend fun invoke(parameter: T): R
    interface GetImagesInfoList : Repository<Unit, LiveData<List<ImageInfo>>>
    interface ReLoadImagesInfoList : Repository<Unit, Result<Unit>>
    interface FindImageInfoById : Repository<String, LiveData<ImageInfo?>>
    interface SetImageSavedFilePathInteractor : Repository<ImageIdFilePath, Unit>

}


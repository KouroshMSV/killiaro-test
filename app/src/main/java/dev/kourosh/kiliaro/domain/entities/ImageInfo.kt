package dev.kourosh.kiliaro.domain.entities

import org.threeten.bp.LocalDateTime

data class ImageInfo(
    val id: String,
    val userId: String,
    val mediaType: String,
    val filename: String,
    val size: Long,
    val createdAt: LocalDateTime,
    val takenAt: String?,
    val guessedTakenAt: String?,
    val contentType: String,
    val thumbnailUrl: String,
    val downloadUrl: String,
    val resx: Long,
    val resy: Long,
    val localSavedPath: String?

)